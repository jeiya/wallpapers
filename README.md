I will try to include the sources

# Unixporn Discord

2zey8h9nktj41.png                        https://discord.com/channels/635612648934735892/635625973764849684/683409849127665746

Berserk_022_eclipse.breaker_by_anato_finnstark.jpg        https://discord.com/channels/635612648934735892/635625973764849684/683528845936164927

Berserk_027_skullknight_by_anato_finnstark.jpg            https://discord.com/channels/635612648934735892/635625973764849684/683528863635996716

berserk-fight.jpg                        https://discord.com/channels/635612648934735892/635625973764849684/683528911631679583

berserk-guts-enter-foggy-forest-painting.jpg            https://discord.com/channels/635612648934735892/635625973764849684/683528948688486440

moon-wallpaper-original-image-by-uajamesmccarthy-19201080.jpg    https://discord.com/channels/635612648934735892/635625973764849684/683554990882685005

rose.jpg                            https://discord.com/channels/635612648934735892/635625973764849684/683738007248830494

# Git Repository

.gitmodules

# Wallhavencc

https://wallhaven.cc/w/z8r2mv

https://wallhaven.cc/w/1k2yv9

https://wallhaven.cc/w/weyexp            

https://wallhaven.cc/w/x6l693

https://wallhaven.cc/w/9m32jx

https://wallhaven.cc/w/j365wm

https://wallhaven.cc/w/x6le2o

https://wallhaven.cc/w/57prk7

https://wallhaven.cc/w/k7le37

https://wallhaven.cc/w/k7kme6

https://wallhaven.cc/w/8535j1

https://wallhaven.cc/w/5glq99

https://wallhaven.cc/w/o59vjp

https://wallhaven.cc/w/zymyvj

https://wallhaven.cc/w/d6g8ml

https://wallhaven.cc/w/g7l1xl

https://wallhaven.cc/w/x8j1zo

https://wallhaven.cc/w/vqg2r8

https://wallhaven.cc/w/7p39r9

https://wallhaven.cc/w/rr2rem

https://wallhaven.cc/w/6oejgq

https://wallhaven.cc/w/dp9klg

https://wallhaven.cc/w/28pdry

https://wallhaven.cc/w/28q9mm

https://wallhaven.cc/w/j361my

https://wallhaven.cc/w/z8557g

https://wallhaven.cc/w/5gyyy1

https://wallhaven.cc/w/d6g58j

https://wallhaven.cc/w/x819vo

https://wallhaven.cc/w/9mkxek

https://wallhaven.cc/w/vqgp1p

https://wallhaven.cc/w/z853dv

https://wallhaven.cc/w/wq2k9r

https://wallhaven.cc/w/1p3k2v

https://wallhaven.cc/w/5d3dm8

https://wallhaven.cc/w/m3pqp9

https://wallhaven.cc/w/dpzogl

https://wallhaven.cc/w/p9kmj3

https://wallhaven.cc/w/3lzy2v

https://wallhaven.cc/w/m9e3dk

https://wallhaven.cc/w/d6g3e

https://wallhaven.cc/w/85o69o

https://wallhaven.cc/w/5weyz8

https://wallhaven.cc/w/7p32mo

https://wallhaven.cc/w/y819ek

https://wallhaven.cc/w/9d5x5k

https://wallhaven.cc/w/y8kvgx

https://wallhaven.cc/w/kx939d

https://wallhaven.cc/w/j5l6r5

https://wallhaven.cc/w/6opqyx

https://wallhaven.cc/w/z859gy

https://wallhaven.cc/w/x81lyv

https://wallhaven.cc/w/m9gpy8

https://wallhaven.cc/w/2yeyjg

https://wallhaven.cc/w/y8vx6l

https://wallhaven.cc/w/9d6owx

https://wallhaven.cc/w/vqg93m

https://wallhaven.cc/w/exy81l

https://wallhaven.cc/w/yxm8wx

https://wallhaven.cc/w/kxwxdq

https://wallhaven.cc/w/9mwzwk

https://wallhaven.cc/w/rdl9vw

https://wallhaven.cc/w/l8myo2

https://wallhaven.cc/w/p989gm

https://wallhaven.cc/w/we5rpq

https://wallhaven.cc/w/28xo8g

https://wallhaven.cc/w/j3py9w

https://wallhaven.cc/w/eyj8dk

https://wallhaven.cc/w/dpxyrj

https://wallhaven.cc/w/vqg9vp

https://wallhaven.cc/w/ox76rp

# Wallpaperboat

green-aesthetic-wallpaper-for-pc.jpg                https://wallpaperboat.com/wp-content/uploads/2020/04/green-aesthetic-wallpaper-for-pc.jpg    

green-aesthetic-wallpaper.jpg                    https://wallpaperboat.com/wp-content/uploads/2020/04/green-aesthetic-wallpaper.jpg

green-aesthetic-wallpaper-1920x1080-9.jpg            https://wallpaperboat.com/wp-content/uploads/2020/04/green-aesthetic-wallpaper-1920x1080-9.jpg    

green-aesthetic-wallpaper-1920x1080-1.jpg            https://wallpaperboat.com/wp-content/uploads/2020/04/green-aesthetic-wallpaper-1920x1080-1.jpg

green-aesthetic-wallpaper-desktop-11.jpg            https://wallpaperboat.com/wp-content/uploads/2020/04/green-aesthetic-wallpaper-desktop-11.jpg

green-aesthetic-wallpaper-download-1.jpg            https://wallpaperboat.com/wp-content/uploads/2020/04/green-aesthetic-wallpaper-download-1.jpg

green-aesthetic-wallpaper-download.jpg                https://wallpaperboat.com/wp-content/uploads/2020/04/green-aesthetic-wallpaper-download.jpg

green-aesthetic-wallpaper-download-full-6.jpg            https://wallpaperboat.com/wp-content/uploads/2020/04/green-aesthetic-wallpaper-download-full-6.jpg

green-aesthetic-wallpaper-download-full-18.jpg            https://wallpaperboat.com/wp-content/uploads/2020/04/green-aesthetic-wallpaper-download-full-18.jpg

green-aesthetic-wallpaper-download-full.jpg            https://wallpaperboat.com/wp-content/uploads/2020/04/green-aesthetic-wallpaper-download-full.jpg

green-aesthetic-wallpaper-free-12.jpg                https://wallpaperboat.com/wp-content/uploads/2020/04/green-aesthetic-wallpaper-free-12.jpg

green-aesthetic-wallpaper-free.jpg                https://wallpaperboat.com/wp-content/uploads/2020/04/green-aesthetic-wallpaper-free.jpg

green-aesthetic-wallpaper-free-download-5.jpg            https://wallpaperboat.com/wp-content/uploads/2020/04/green-aesthetic-wallpaper-free-download-5.jpg

green-aesthetic-wallpaper-free-download.jpg            https://wallpaperboat.com/wp-content/uploads/2020/04/green-aesthetic-wallpaper-free-download.jpg

green-aesthetic-wallpaper-full-hd.jpg                https://wallpaperboat.com/wp-content/uploads/2020/04/green-aesthetic-wallpaper-full-hd.jpg

green-aesthetic-wallpaper-hd-10.jpg                https://wallpaperboat.com/wp-content/uploads/2020/04/green-aesthetic-wallpaper-hd-10.jpg

green-aesthetic-wallpaper-image-2.jpg                https://wallpaperboat.com/wp-content/uploads/2020/04/green-aesthetic-wallpaper-image-2.jpg

green-aesthetic-wallpaper-pic-19.jpg                https://wallpaperboat.com/wp-content/uploads/2020/04/green-aesthetic-wallpaper-pic-19.jpg

# Reddit

asthetic1     https://www.reddit.com/r/desktops/comments/156ogi2/some_nice_asthetic_wallpapers/?utm_source=share&utm_medium=web2x&context=3

asthetic2     https://www.reddit.com/r/desktops/comments/156ogi2/some_nice_asthetic_wallpapers/?utm_source=share&utm_medium=web2x&context=3

asthetic3     https://www.reddit.com/r/desktops/comments/156ogi2/some_nice_asthetic_wallpapers/?utm_source=share&utm_medium=web2x&context=3

nixos_edited    https://www.reddit.com/r/NixOS/comments/1632nml/edited_this_into_a_nix_wallpaper_a_while_ago/?utm_source=share&utm_medium=ios_app&utm_name=ioscss&utm_content=1&utm_term=1

InfinityNikki

https://www.reddit.com/r/InfinityNikki/comments/1hsbxac/i_really_love_the_pink_evolution_of_the_waves/

# Wallpaperflare

HD wallpaper: city, digital, light, design, technology, laser, 3d, wallpaper.jpg        https://www.wallpaperflare.com/city-digital-light-design-technology-laser-3d-wallpaper-wallpaper-gfyk/download

HD wallpaper: green leafed tree, landscape, nature, lake, trees, mountains.jpg            https://www.wallpaperflare.com/green-leafed-tree-landscape-nature-lake-trees-mountains-wallpaper-pmtwz/download/1920x1080

HD wallpaper: snow-capped mountain, snow covered mountains at golden hour, landscape.jpg    https://www.wallpaperflare.com/snow-capped-mountain-snow-covered-mountains-at-golden-hour-landscape-wallpaper-tk/download

https://www.wallpaperflare.com/buildings-illustration-animated-pink-and-black-buildings-illustration-wallpaper-suf/download/1920x1200

https://www.wallpaperflare.com/silhouette-of-steel-ridge-wallpaper-blue-and-pink-sky-painting-wallpaper-pcm/download/1920x1200

https://www.wallpaperflare.com/digital-digital-art-artwork-illustration-drawing-digital-painting-wallpaper-gjrrx/download/1920x1200

https://www.wallpaperflare.com/silhouette-of-mountain-simple-simple-background-minimalism-wallpaper-phxwd/download/1920x1200

https://www.wallpaperflare.com/solar-eclipse-illustration-black-hole-digital-wallpaper-abstract-wallpaper-hfd/download/1920x1200

https://www.wallpaperflare.com/pink-and-brown-trees-digital-wallpaper-artwork-fantasy-art-wallpaper-ptkah/download/1920x1200

https://www.wallpaperflare.com/calm-body-of-water-backlit-chiemsee-dawn-desktop-backgrounds-wallpaper-aovgs/download/1920x1200

https://www.wallpaperflare.com/pink-mountain-terrain-illustration-mountains-low-poly-minimalism-wallpaper-zfs/download/1920x1200

https://www.wallpaperflare.com/synthwave-background-music-sunrise-abstract-sunset-pink-wallpaper-bgxcp/download/1920x1200

https://www.wallpaperflare.com/pink-sakura-tree-wallpaper-sunset-fantasy-art-lava-trees-wallpaper-sslyt/download/1920x1200

https://www.wallpaperflare.com/two-dragons-flying-digital-wallpaper-fantasy-art-sky-anime-wallpaper-ecg/download/1920x1200

https://www.wallpaperflare.com/flowering-tree-illustration-minimalism-digital-art-trees-sun-wallpaper-prvnz/download/1920x1200

# Alpha Coders

https://mobile.alphacoders.com/wallpapers/view/1012914
https://mobile.alphacoders.com/wallpapers/view/777074
https://mobile.alphacoders.com/wallpapers/view/714294
https://mobile.alphacoders.com/wallpapers/view/859168
https://mobile.alphacoders.com/wallpapers/view/772504
https://mobile.alphacoders.com/wallpapers/view/1012912
